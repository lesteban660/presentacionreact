import './App.css'
import ComponenteDos from './Components/ComponenteDos'
import ComponenteTres from './Components/ComponenteTres'
import ComponenteUno from './Components/ComponenteUno'



function App() {

  return (
    <>
    
    <ComponenteUno/>
    <ComponenteDos />

    < ComponenteTres />
    </>
    
  )
}

export default App
