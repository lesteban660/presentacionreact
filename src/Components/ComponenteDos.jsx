import React, {Component} from 'react'
import img1 from '../assets/img.jpg'
import './ComponenteDos.css'

class ComponenteDos extends Component {
  constructor() {
    super();
    this.state = { checked: false };
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(checked) {
    this.setState({ checked });
  }
  render() {
    return (
    <div className='cont'>
      <h1 className='datos'>Datos</h1>
      <img src={img1} width="150" height="150" />
      <div className='card-container'>

        <button className='card card-small' onClick={() => this.handleChange(false)}>
        <svg className='material-icons' xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M19 13h-6v6h-2v-6H5v-2h6V5h2v6h6v2z" /></svg>
        <span>Nombre</span>
        </button>

        <button className='card card-small' onClick={() => this.handleChange(false)}>
        <svg className="material-icons" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M19 13h-6v6h-2v-6H5v-2h6V5h2v6h6v2z"/></svg>
          <span>Gustos</span>
        </button>

        <button className='card card-small' onClick={() => this.handleChange(false)}>
        <svg className="material-icons" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M19 13h-6v6h-2v-6H5v-2h6V5h2v6h6v2z"/></svg>
          <span>Pasatiempos</span>
        </button>

        <button className='card card-small' onClick={() => this.handleChange(false)}>
        <svg className="material-icons" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M19 13h-6v6h-2v-6H5v-2h6V5h2v6h6v2z"/></svg>
          <span>Materia</span>
        </button>

        <button className='card card-small' onClick={() => this.handleChange(false)}>
        <svg className="material-icons" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M19 13h-6v6h-2v-6H5v-2h6V5h2v6h6v2z"/></svg>
          <span>Correo</span>
        </button>

        <button className='card card-small' onClick={() => this.handleChange(false)}>
        <svg className="material-icons" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M19 13h-6v6h-2v-6H5v-2h6V5h2v6h6v2z"/></svg>
          <span>Carrera</span>
        </button>

        <button className='card card-small' onClick={() => this.handleChange(false)}>
        <svg className="material-icons" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M19 13h-6v6h-2v-6H5v-2h6V5h2v6h6v2z"/></svg>
          <span>Matricula</span>
          </button>

      </div>

      
      <div className='terminal' switch={() => this.handleChange(false)}>
          <pre witchCase="'component'">Lopez Gallardo Esteban</pre>
      </div>
</div>
  )
    }
}

export default ComponenteDos

