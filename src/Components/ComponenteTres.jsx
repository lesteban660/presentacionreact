import React from 'react'
import './ComponenteTres.css'
import github from '../assets/github.png'
import instagram from '../assets/instagram.png'
import facebook from  '../assets/facebook.png'
import gitlab from '../assets/gitlab.png'

function ComponenteTres() {
  return (
    <body className="conta">
    
    <>
    <div className="facebook">
      
      <a href="https://www.facebook.com/eztevan.lopesgayardo?mibextid=ZbWKwL" target="_blank">
        <img src={facebook}/> <span>....</span>
      </a>
      <a href="https://instagram.com/esteban_lopez_g?igshid=ZDdkNTZiNTM=" target="_blank">
        <img src={instagram} /> <span>....</span>
      </a>
      <a href="https://github.com/esteban-hash" target="_blank">
        <img src={github}/> <span>....</span>
      </a>
      <a href="https://gitlab.com/lesteban660" target="_blank">
        <img src={gitlab} /> <span>....</span>
      </a>
      
    </div>
    </>
  </body>
  )
}

export default ComponenteTres